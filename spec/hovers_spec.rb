
describe 'Mouse Hover', :hover do

    before(:each) do
        visit '/hovers'
    end

    it 'Quando passo o mouse sobre o Blade' do
        card = find('img[alt=Blade]')
        card.hover

        expect(page).to have_content 'Nome: Blade'
    end

    it 'Quando passo o mouse sobre o Pantera Negra' do
        card = find('img[alt="Pantera Negra"]') # passar dentro de sting quando tem espaço ou caracteres especiais
        # card = find('img[alt^=Pantera]') # ^ = começa 
        # card = find('img[alt$=Negra]') # $ = termina
        # card = find('img[alt*=Pantera]') # * = contem . Exemplos acima foram usados expressoes regulares
        card.hover

        expect(page).to have_content 'Nome: Pantera Negra'
    end

    it 'Quando passo o mouse sobre o Homem Aranha' do
        card = find('img[alt="Homem Aranha"]')
        card.hover

        expect(page).to have_content 'Nome: Homem Aranha'
    end

end
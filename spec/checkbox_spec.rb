
describe 'Caixas de seleção', :checkbox do

    before(:each) do
        visit '/checkboxes'
    end

    it 'Marcando uma opção' do
        check('thor') # marcando checkbox através do id
    end

    it 'Desmarcando uma opção' do
        uncheck('antman') # desmarcando checkbox através do name
    end

    it "Mancando com find set true" do
        find('input[value=cap]').set(true) 
    end

    it 'Desmarcando com find set false' do
        find('input[value=guardians').set(false)
    end

end

describe 'Forms', :login do

    before(:each) do
        visit '/login'
    end

    it 'Login com sucesso'  do
        fill_in 'userId', with: 'stark'
        fill_in 'passId', with: 'jarvis!'
        
        click_button 'Login'

        expect(find('#flash').visible?).to be true

        # expect(find('#flash').text).to include 'Olá, Tony Stark. Você acessou a área logada!'
        expect(find('#flash')).to have_content 'Olá, Tony Stark. Você acessou a área logada!'
    end

    it 'Senha incorreta' do
        fill_in 'userId', with: 'stark'
        fill_in 'passId', with: 'jarvis'
        
        click_button 'Login'

        expect(find('#flash').visible?).to be true
        expect(find('#flash')).to have_content 'Senha é invalida!'
    end
  
  
    it 'Usuário não cadastrado' do
        fill_in 'userId', with: 'gustavo'
        fill_in 'passId', with: 'naotemsenha'
        
        click_button 'Login'

        expect(find('#flash').visible?).to be true
        expect(find('#flash')).to have_content 'O usuário informado não está cadastrado!'
    end

end
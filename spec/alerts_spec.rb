
describe('Alert', :alert) do
    
    before(:each) do
        visit '/javascript_alerts'
    end

    it 'Alerta' do
        find('.btn-success'). click

        msg = page.driver.browser.switch_to.alert.text
        expect(msg).to eql 'Isto é uma mensagem de alerta' 
    end

    it 'Confirmar' do
        click_button 'Confirma'

        msg = page.driver.browser.switch_to.alert.text
        expect(msg).to eql 'E ai confirma?'

        page.driver.browser.switch_to.alert.accept
        expect(page).to have_content 'Mensagem confirmada'
    end

    it 'Não Confirmar' do
        click_button 'Confirma'

        msg = page.driver.browser.switch_to.alert.text
        expect(msg).to eql 'E ai confirma?'

        page.driver.browser.switch_to.alert.dismiss
        expect(page).to have_content 'Mensagem não confirmada'
    end

    it 'Accept Prompt', :accept_prompt do
        accept_prompt(with: 'Gustavo') do
            click_button 'Prompt'
        end

        expect(page).to have_content 'Olá, Gustavo'
    end

    it 'Cancela Prompt', :dismiss_prompt do
        dismiss_prompt(with: 'Gustavo') do
            click_button 'Prompt'
        end

        expect(page).to have_content 'Olá, null'
    end

end
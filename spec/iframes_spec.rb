
describe 'Iframes' do

    describe 'Bom', :iframe_bom do

        before(:each) do
            visit '/nice_iframe'
        end

        it 'Adicionar ao carrinho' do
            within_frame('burgerId') do
                produto = find('.menu-item-info-box', text: 'BATATAS FRITAS')
                produto.find('a').click
                expect(find('#cart')).to have_content 'R$ 5,50'
            end
        end

    end

    describe 'Ruim', :iframe_ruim do

        before(:each) do
            visit '/bad_iframe'
        end

        it 'Carrinho deve estar vazio' do
            script = '$(".box-iframe").attr("id", "tempId");'
            page.execute_script(script)

            within_frame('tempId') do
                expect(find('#cart')).to have_content 'Seu carrinho está vazio!'
            end
        end

    end

end
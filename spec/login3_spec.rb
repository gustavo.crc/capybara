
describe 'Login com cadastro', :login3 do

    before(:each) do
        visit '/access'
    end

    it 'Cadastro com id dinamico'  do
        find('input[id$=UsernameInput]').set 'stark'
        find('input[id^=PasswordInput]').set 'jarvis!'

        click_link 'Criar Conta'

        expect(page).to have_content 'Dados enviados. Aguarde aprovação do seu cadastro!'
    end

    it 'Login com sucesso' do
        within('#login') do
            find('#usernameId'). set 'stark'
            find('#passwordId'). set 'jarvis!'
            click_button 'Entrar'
        end
        
        expect(find('#flash')).to have_content 'Olá, Tony Stark. Você acessou a área logada!'
    end 

end
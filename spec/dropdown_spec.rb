
describe 'Caixa de opções', :dropdown do

    before(:each) do
        visit '/dropdown'
    end

    it 'Item especifico simples' do
        select('Bruce Banner', from: 'dropdown') # exclusivo para select com id
    end

    it 'Item especifico com o find' do
        drop = find('.avenger-list')
        drop.find('option', text: 'Steve Rogers').select_option
    end

    it "Qualquer item" do
        drop = find('.avenger-list')
        drop.all('option').sample.select_option # .all = todos os elementos .sample = escolhe uma qualquer
    end

end
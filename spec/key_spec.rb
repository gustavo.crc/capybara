
describe "Key Presses", :key do

    before(:each) do
        visit '/key_presses'
    end

    it 'Enviando teclas' do
        teclas = %i[tab escape space enter shift control alt]

        teclas.each do |t|
            find('#campo-id').send_keys t
            expect(page).to have_content 'You entered: ' + t.to_s.upcase
        end  
    end

    it "Enviando letras" do
        letras = %w[a b c d e f g h i j k l m n o t u v w x z]

        letras.each do |l|
            find('#campo-id').send_keys l
            expect(page).to have_content 'You entered: ' + l.to_s.upcase
        end
    end

end
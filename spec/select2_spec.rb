
describe('Single', :single) do

    before(:each) do
        visit '/apps/select2/single.html'
    end

    it 'Seleciona ator por nome' do
        find('.select2-selection__arrow').click
        find('.select2-results__option', text: 'Jim Carrey').click
    end

    it 'Busca e clica no ator' do
        find('.select2-selection__arrow').click
        find('.select2-search__field').set 'Kevin James'
        find('.select2-results__option', text: 'Kevin James').click
    end

end

describe('Multiple', :multiple) do
    
    before(:each) do
        visit '/apps/select2/multi.html'
    end

    def selecione(ator)
        find('.select2-selection__rendered').click
        find('.select2-search__field').set ator
        find('.select2-results__option').click
    end

    it 'Selecionar mais de um ator' do
        atores = ['Jim Carrey', 'Kevin James', 'Adam Sandler']

        atores.each do |a|
            selecione(a)
        end
    end

end

describe 'Data Table', :table do

    before(:each) do
        visit 'tables'
    end

    it 'Deve exibir o salário do Stark' do
        atores = all('table tbody tr')
        stark = atores.detect { |ator| ator.text.include?('Robert Downey Jr') }

        expect(stark.text).to include '10.000.000'
    end

    it 'Deve exibir o salário do Stark' do 
        vin = find('table tbody tr', text: '@vindiesel')
        expect(vin).to have_content '10.000.000'
    end

    it 'Deve exibir o filme velozes' do
        vin = find('table tbody tr', text: '@vindiesel')
        movie = vin.all('td')[2].text

        expect(movie).to eql 'Velozes e Furiosos'
    end

    it 'Deve exibir o insta do Chris Evans' do
        evans = find('table tbody tr', text: 'Chris Evans')
        insta = evans.all('td')[4].text

        expect(insta).to eql '@teamcevans'
    end

    it 'Deve selecionar Vin Diesel para remoção' do
        vin = find('table tbody tr', text: '@vindiesel')
        vin.find('a', text: 'delete').click

        msg = page.driver.browser.switch_to.alert.text
        expect(msg).to eql 'Vin Diesel foi selecionado para remoção!'
    end

    it 'Deve selecionar Vin Diesel para edição' do
        vin = find('table tbody tr', text: '@vindiesel')
        vin.find('a', text: 'edit').click

        msg = page.driver.browser.switch_to.alert.text
        expect(msg).to eql 'Vin Diesel foi selecionado para edição!'
    end
 
end

describe 'Upload', :upload do

    before(:each) do
        visit '/upload'

        @arquivo = Dir.pwd + '/spec/fixture/ferrari.txt'
        @imagem = Dir.pwd + '/spec/fixture/ferrari.jpg'
    end

    it 'Upload com arquivo txt' do
        attach_file('file-upload', @arquivo)

        click_button 'Upload'
        div_arquivo = find('#uploaded-file')
        expect(div_arquivo.text).to eql 'ferrari.txt'
    end

    it 'Upload com arquivo jpg' do
        attach_file('file-upload', @imagem)
        click_button 'Upload'

        img = find('#new-image')
        expect(img[:src]).to include '/uploads/ferrari.jpg'
    end

end
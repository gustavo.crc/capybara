
describe 'Botões de radio', :radio do

    before(:each) do
        visit '/radios'
    end

    it 'Seleção por ID' do
        choose('cap')
    end

    it 'Seleção por find e css selector' do
        find('input[value=guardians]').click
    end

end